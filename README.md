# README #

Comparador de bases para NodeJs

### Instalação ###

npm install git+https://[usuario]@bitbucket.org/megasistemas/comparador-banco.git (substituir [usuario] pelo seu nome de usuário no BitBucket)

### Uso ###


```
#!javascript
var comparador = require('comparador-banco')

var connectData = {
    hostname: "pc_exemplo",
    port: 1521,
    database: "orc1", // System ID (SID)
    user: "mega",
    password: "megamega"
}

comparador.executa(connectData, './orquestrador.json')
```

