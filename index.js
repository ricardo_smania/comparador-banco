﻿var fs = require('fs');

var oracle = require('oracle');

var connectData = {
    hostname: "srv_2008",
    port: 1521,
    database: "orc1", // System ID (SID)
    user: "mgrh",
    password: "megarh"
};

var _ = require('underscore')

function executa(connectData, ct, arquivosel, arquivores, max_results, callback)
{
    oracle.connect(connectData, function(err, connection) {

        if (err) {
            console.log("Error connecting to db:", err);
        }

        connection.setPrefetchRowCount(1000);

        connection.execute(arquivosel, [], function(err, results) {

            //console.log('executou SQL')

            if (err) {
                    console.log("Error executing query:", err);
                    return;
            }

            resultadoselect = JSON.stringify(results, null, 2);

            gravaArquivo(arquivores, resultadoselect)

            connection.close();

            compara(ct.arq_baseline, results, ct.arq_resultado_conf, ct.arq_baseline_conf, max_results, function(resultado, erro) {
                //resultadoJson = JSON.stringify(resultado, null, 2);
                //gravaArquivo(arquivores, resultadoJson)
                callback(resultado, erro)
            })
        }); 
        return;
    });
}

function gravaArquivo(arquivores, resultadoselect)
{
    fs.writeFile(arquivores, resultadoselect, function (err) {
        if (err) return console.log(err);
    });
}

function diferencaArray(base, retorno)
{
    return base.filter(function(item)
    {
        valorBase = retorno.filter(function(itemBase)
        {
            return itemBase.ID === item.ID
        })
        return valorBase.length == 0
    })
}

function comparaObjetos(base, outro, max_results)
{
    //console.log('começou comparação')
    resultado = {}
    resultado.diferencasBase = []
    resultado.diferencasOutro = []
    resultado.tamanhoBase = base.length
    resultado.tamanhoRetorno = outro.length
    resultado.diferencas = []
    resultado.somenteBase = []
    resultado.somenteOutro = []

    var linhaBase = 0
    var linhaOutro = 0

    base = _.sortBy(base, "ID")
    //console.log('ordenou baseline ' + base.length)
    console.log('Quantidade de registros versão atual: ' + outro.length)
    outro = _.sortBy(outro, "ID")
    //console.log('ordenou versao atual ' + outro.length)
    console.log('Quantidade de registros baseline: ' + base.length)


    var itemBase
    var itemOutro

    while (linhaBase < base.length || linhaOutro < outro.length)
    {

        if (linhaBase < base.length)
            itemBase = base[linhaBase]

        if (linhaOutro < outro.length)
            itemOutro = outro[linhaOutro]

        if (itemBase.ID === itemOutro.ID)
        {
            resultado.diferencasBase.push(itemBase)
            resultado.diferencasOutro.push(itemOutro)

            if (resultado.diferencas.length < max_results)
            {
                if (!_.isEqual(itemBase, itemOutro))
                {
                    var valoresBase = {}
                    var valoresOutro = {}
                    var teve_diferenca = false
                    for (campo in itemBase)
                    {                    
                        if (itemBase[campo] != itemOutro[campo])
                        {
                            if (itemOutro[campo] instanceof Date)
                            {
                                if (Date.parse(itemBase[campo]) != Date.parse(itemOutro[campo]))
                                {
                                    teve_diferenca = true
                                    valoresBase[campo] = itemBase[campo]
                                    valoresOutro[campo] = itemOutro[campo]    
                                }
                            }
                            else
                            {
                                teve_diferenca = true
                                valoresBase[campo] = itemBase[campo]
                                valoresOutro[campo] = itemOutro[campo]    
                            }
                            
                        }
                        
                    }
                    if (teve_diferenca)
                    {
                        diferenca = {ID: itemBase.ID, base: valoresBase, outro: valoresOutro}
                        resultado.diferencas.push(diferenca)
                    }
                }
            }
            linhaBase++
            linhaOutro++
        }
        else
        {

            if (itemBase.ID < itemOutro.ID)
            {
                resultado.diferencasBase.push(itemBase)
                resultado.diferencasOutro.push({linhas:Object.keys(itemBase).length})
                if (resultado.somenteBase.length < max_results)
                    resultado.somenteBase.push(itemBase)
                linhaBase++
            }
            else
            {
                resultado.diferencasBase.push({linhas:Object.keys(itemOutro).length})
                resultado.diferencasOutro.push(itemOutro)
                if (resultado.somenteOutro.length < max_results)
                    resultado.somenteOutro.push(itemOutro)
                linhaOutro++
            }
        }

        
    }

    //console.log('terminou comparação')
    console.log('Divergências: ' + resultado.diferencas.length)
    console.log('                               ')
    console.log('                               ')
    console.log('-------------------------------')


    //resultado.iguais = (base.length == outro.length && resultado.diferencas.length + resultado.somenteBase.length + resultado.somenteResultado.length == 0)
    return resultado
}

function compara(caminhoBase, outro, outroConf, caminhoBaseConf, max_results, callback)
{
    fs.readFile(caminhoBase, {encoding: 'utf8'}, function(err, conteudo) {
        if (err)
        {            
            console.log(err)
            throw err
        }

        else
        {
            base = JSON.parse(conteudo)
            resultado = comparaObjetos(base, outro, max_results)
            insere_linhas(resultado.diferencasBase);
            fs.writeFile(caminhoBaseConf,insere_linhas(resultado.diferencasBase))
            fs.writeFile(outroConf,insere_linhas(resultado.diferencasOutro))
            callback(resultado, null)
            //console.log(JSON.stringify(resultado, null, 2))
        }
        
    })
}

function insere_linhas(resultado){
    var resFormat = JSON.stringify(resultado,null,2)
    var myRegexp = /\"linhas\": (\d+)/g;
    var match = myRegexp.exec(resFormat);
    if (match){
        var linhas = match[1]
        var resFormatLinha = resFormat.replace(myRegexp,Array(parseInt(linhas)).join("\n"))
        return resFormatLinha    
    }
    else{
        return resFormat
    }
    
}

function executa_teste(ct, arquivosel, arquivores, arquivoBase, max_results) {
    it(ct.titulo, function(done) {
        console.log(ct.titulo) 

        arquivosel = fs.readFileSync(ct.arq_select,"utf-8");
        arquivores = ct.arq_resultado;
        arquivoinesperados = ct.titulo

        executa(connectData, ct, arquivosel, arquivores, max_results, function(resultado, erro) {

            //console.log(JSON.stringify(resultado, null, 2))
            //fs.writeFile("./inesperados/"+ arquivoinesperados+".json",JSON.stringify(resultado.somenteOutro,null,2))
            resultado.diferencas.length.should.equal(0)
            resultado.somenteBase.length.should.equal(0)
            resultado.somenteOutro.length.should.equal(0)               
                                
            done()
        });
    })
}

function executa_testes(connectData, caminho_arquivo_ct, max_results, usa_mocha) {

    if (!connectData)
        throw "Favor informar dados de conexão"


    if (!caminho_arquivo_ct)
        throw "Favor informar caminho do arquivo com os casos de teste"

    var executou

    var conteudo = fs.readFileSync(caminho_arquivo_ct, "utf-8")

    var casos_teste = JSON.parse(conteudo);
    var ct;
    var arquivosel;
    var arquivores;
    var arquivoBase;

    if (usa_mocha)
    {

        describe('teste', function() {

            this.timeout(200000)

            should = require('should')

            for (ind in casos_teste){

                ct = casos_teste[ind];

                executa_teste(ct, arquivosel, arquivores, arquivoBase, max_results)

            }
        })
    }
}

module.exports = {
    executa: executa_testes/*,
    compara: comparaObjetos*/
}
